/////
// CSE 02 InsertionSort
// Jennifer Feeney
// December 07, 2018
/* this program runs the insertion sorting processes on an array */
////

//importing the array class into the program
import java.util.Arrays;

public class InsertionSort{ 
  //main method necessary for every program
  public static void main(String[] args) {
    
    //initializing the two arrays
    int[] bestArray = {1, 2, 3, 4, 5, 6, 7, 8, 9};
    int[] worstArray = {9, 8, 7, 6, 5, 4, 3, 2, 1};
    
    //performing selection sort on the two arrays
    int bestNum = insertionSort(bestArray);
    int worstNum = insertionSort(worstArray);
    
    //printing out the number of iterations for each array
    System.out.println("The total number of operations performed on the sorted array is " + bestNum);
    System.out.println("The total number of operations performed on the reverse sorted array is " + worstNum);
    
  } //end of main method
  
  public static int insertionSort(int[] array) {
    
    System.out.println(Arrays.toString(array)); //prints out initial array
    
    //initializing variables
    int placeholder = 0; //temporary value for swap
    int counter = 0;
    
    for(int i = 1; i < array.length; i++){
      counter++; //update the iterations counter
      
      //insert array[i] into a sorted subarray[0...i-1] so that array[0...i] is sorted
      for(int j = i; j > 0; j--){
        
        //swap the values if the previous value is greater than the current one
        if(array[j] < array[j-1]){
          placeholder = array[j];
          array[j] = array[j-1];
          array[j-1] = placeholder;
          counter++; //update the iterations counter
        }else{
          break; //if the two values are equal or already sorted in decending order, there's no need to compare the other values
        }
      }
      
      System.out.println(Arrays.toString(array)); //prints out sorted array at each step
    }
    
    return counter;
    
  } //end of insertionSort method
  
} //end of class