/////
// CSE 02 SelectionSort
// Jennifer Feeney
// December 07, 2018
/* this program runs the selection sorting processes on an array */
////

//importing the array class into the program
import java.util.Arrays;

public class SelectionSort{ 
  //main method necessary for every program
  public static void main(String[] args) {
    
    //initializing the two arrays
    int[] bestArray = {1, 2, 3, 4, 5, 6, 7, 8, 9};
    int[] worstArray = {9, 8, 7, 6, 5, 4, 3, 2, 1};
    
    //performing selection sort on the two arrays
    int bestNum = selectionSort(bestArray);
    int worstNum = selectionSort(worstArray);
    
    //printing out the number of iterations for each array
    System.out.println("The total number of operations performed on the sorted array is " + bestNum);
    System.out.println("The total number of operations performed on the reverse sorted array is " + worstNum);
    
  } //end of main method
  
  public static int selectionSort(int[] array) {
    
    System.out.println(Arrays.toString(array)); //prints out initial array
    
    //initializing variables
    int min = 0, minIndex = 0;
    int placeholder = 0; //temporary value for swap
    int counter = 0;
    
    for(int i = 0; i < array.length - 1; i++){
      counter++; //update the iterations counter
      
      //STEP 1: Find the minimum in the array[i...array.length-1]
      min = array[i];
      minIndex = i;
      for(int j = i + 1; j < array.length; j++){
        counter++; //update the iterations counter
        
        //if there's a number less than the current minimum value further along in the array, make that to the minimum value
        if(array[j] < min){
          min = array[j];
          minIndex = j;
          
        }
      }
      
      //STEP 2: Swap array[i] with the minimum found above
      if(minIndex != i){
        placeholder = array[i];
        array[i] = min;
        array[minIndex] = placeholder;
        System.out.println(Arrays.toString(array)); //prints out sorted array at each step
      }
      
      
    }
    
    return counter;
    
  } //end of selectionSort method
  
} //end of class