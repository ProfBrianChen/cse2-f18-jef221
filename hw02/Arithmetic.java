//////////////
//// CSE 02 Cyclometer
/// Jennifer Feeney
/// September 7, 2018
//
// this program calculates the sales tax and total costs for pants, shirts and belts in Pennsylvania
//
public class Arithmetic {
  //main method required for every Java program
  public static void main(String[] args) {
    
    //assumptions or input variables
    int numPants = 3; //number of pairs of pants
    double pantsPrice = 34.98; //cost per pair of pants
    int numShirts = 2; //number of sweatshirts
    double shirtPrice = 24.99; //price per shirt
    int numBelts = 1; //number of belts 
    double beltPrice = 33.99; //cost per belt 
    double paSalesTax = 0.06; //the tax rate in Pennsylvania
    
    //decalring variables for output calculations
    double totalCostOfPants; //total cost of pants 
    double totalCostOfShirts; //total cost of shirts
    double totalCostOfBelts; //total cost of belts
    double totalCostOfPurchase; //total cost of the purchase before sales tax
    double salesTaxPants; //sales tax on the pants
    double salesTaxShirts; //sales tax on the shirts
    double salesTaxBelts; //sales tax on the belts
    double totalSalesTax; //the total sales tax on the items in Pennsylvania
    double totalPaid; //total cost of the purchase with sales tax
    
    //run calculations on the total cost of each item, their sales tax, and the totals with and without sales tax
    totalCostOfPants = numPants * pantsPrice;
    totalCostOfPants = (int) (totalCostOfPants * 100) / 100.0; //making sure that the calculations only have two decimal points
    totalCostOfShirts = numShirts * shirtPrice;
    totalCostOfShirts = (int) (totalCostOfShirts * 100) / 100.0; //making sure that the calculations only have two decimal points
    totalCostOfBelts = numBelts * beltPrice;
    totalCostOfBelts = (int) (totalCostOfBelts * 100) / 100.0; //making sure that the calculations only have two decimal points
    totalCostOfPurchase = totalCostOfPants + totalCostOfShirts + totalCostOfBelts;
    totalCostOfPurchase = (int) (totalCostOfPurchase * 100) / 100.0; //making sure that the calculations only have two decimal points
    salesTaxPants = pantsPrice * paSalesTax;
    salesTaxPants = (int) (salesTaxPants * 100) / 100.0; //making sure that the calculations only have two decimal points
    salesTaxShirts = shirtPrice * paSalesTax;
    salesTaxShirts = (int) (salesTaxShirts * 100) / 100.0; //making sure that the calculations only have two decimal points
    salesTaxBelts = beltPrice * paSalesTax;
    salesTaxBelts = (int) (salesTaxBelts * 100) / 100.0; //making sure that the calculations only have two decimal points
    totalSalesTax = salesTaxPants + salesTaxBelts + salesTaxShirts;
    totalSalesTax = (int) (totalSalesTax * 100) / 100.0; //making sure that the calculations only have two decimal points
    totalPaid = totalCostOfPurchase + totalSalesTax;
    totalPaid = (int) (totalPaid * 100) / 100.0; //making sure that the calculations only have two decimal points
    
    //print the results of the calculations to the terminal
    System.out.println("The total cost of the pants is " + totalCostOfPants + " and its total sales tax is " + salesTaxPants + ".");
    System.out.println("The total cost of the shirts is " + totalCostOfShirts + " and its total sales tax is " + salesTaxShirts + ".");
    System.out.println("The total cost of the belt is " + totalCostOfBelts + " and its total sales tax is " + salesTaxBelts + ".");
    System.out.println("The total cost of the purchase before adding the sales tax is " + totalCostOfPurchase + ".");
    System.out.println("The total sales tax is " + totalSalesTax + ".");
    System.out.println("The total cost of the purchase with the sales tax added is " + totalPaid + ".");
    
 
  } //end of main method
  
} //end of class