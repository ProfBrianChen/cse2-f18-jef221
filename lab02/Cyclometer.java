//////////////
//// CSE 02 Cyclometer
/// Jennifer Feeney
/// September 7, 2018
//
// this code comutes the distances for two trips using a seies of variables 
//to output their total distances in miles seperately and then combined
//
public class Cyclometer {
  //main method required for every Java program
  public static void main(String[] args) {
    
    //our input data
    int secsTrip1 = 480; //number of seconds that trip 1 takes
    int secsTrip2 = 3220; //number of seconds that trip 2 takes
    int countsTrip1 = 1561; //number of rotations for trip 1
    int countsTrip2 = 9037; //number of rotations for trip 2
    
    //our intermediate variables and output data
    double wheelDiameter = 27.0, //constant for the diameter of the wheel
    PI = 3.14159, //the number the user will utilize for the variable pi, classified as a double
    feetPerMile = 5280, //constant for the amount of feet for one mile, classified as a double
    inchesPerFoot = 12, //constant for the amount of inches for one foot, classified as a double
    secondsPerMinute = 60; //constant for the number of seconds in one minute, classified as a double
    double distanceTrip1, distanceTrip2, totalDistance; //declaring the distances of each trip and the total trip as a double
    
    //prints out numbers stored as variables for trips 1 and 2
    System.out.println("Trip 1 took " + (secsTrip1/secondsPerMinute) + " minutes and had " + countsTrip1 + " counts.");
    System.out.println("Trip 2 took " + (secsTrip2/secondsPerMinute) + " minutes and has " + countsTrip2 + " counts.");
    
    //runs calculations and stores the values for the distances of the trips and total trip in miles
    distanceTrip1 = countsTrip1 * wheelDiameter * PI;
    //above gives the distance in inches 
    //(for each count, a rotation of the wheel travels
    //the diameter in inches times pi)
    distanceTrip1 /= inchesPerFoot * feetPerMile; //gives the distance in miles
    distanceTrip2 = countsTrip2 * wheelDiameter * PI / inchesPerFoot / feetPerMile;
    totalDistance = distanceTrip1 + distanceTrip2;
    
    //print the output data
    System.out.println("Trip 1 was " + distanceTrip1 + " miles.");
    System.out.println("Trip 2 was " + distanceTrip2 + " miles.");
    System.out.println("The total distance was " + totalDistance + " miles.");
    
  } //end of main method
  
} //end of class