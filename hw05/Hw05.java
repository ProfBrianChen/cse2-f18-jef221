/////
// CSE 02 Hw05
// Jennifer Feeney
// October 09, 2018
////
// this program generates a user-inputed number of five card hands 
// and outputs the probabilities of getting certain poker hands to the screen
////

//importing the scanner class into the program
import java.util.Scanner;

public class Hw05{
	//main method necessary for every program
  public static void main(String[] args){
    
    Scanner myScanner = new Scanner(System.in); //assigns the scanner to the program
    
    //initialize and assign the variables associated with the number of hands generated (counter)
    int counter = 1;
    int maxCount = 0;
    
    /* initialize and assign the variables
    that will check whether or not the user-entered values are the correct type */
    boolean testVal = false;
    String badValue = "";
    String enterValue = "";
    
    while (maxCount <= 0){
      System.out.println("Enter the number of hands you would like to generate: ");
      testVal = myScanner.hasNextInt(); //checks to see whether the user entered an integer
      
      if (!testVal) {
        badValue = myScanner.nextLine(); //the string badValue will take the non-integer the user entered
        System.out.println("You must enter a valid number.");
        continue; //if the user enters a non-integer, it will re-run the while loop until the correct value is entered
      }
      
      maxCount = myScanner.nextInt(); //assigns the integer value to maxCount
      enterValue = myScanner.nextLine(); //assigns the extra space when the user hits the enter key to enterValue
      
    }
    
    //initialize and assign variables associated with the cards within the hand
    int cardNum = 1;
    int faceValue = 0;
    
    //initialize and assign the five cards in a hand
    int card1 = 0;
    int card2 = 0;
    int card3 = 0;
    int card4 = 0;
    int card5 = 0;
    
    //initialize and assign variables associated with the different card probabilities
    int fourOfAKind = 0;
    int threeOfAKind = 0;
    int twoPair = 0;
    int onePair = 0;
    
    /* initialize and assign variables associated with the 
    mechanics of the while loop in order to get the proper output */
    int randomNum = 0;
    int maxValue = 52;
    int minValue = 1;
    
    /* loops around, creating five card hands until 
    it reaches the user allotted number of hands as inputed by the user */
    while (counter <= maxCount) {
      
      //assigning five different cards to a hand
      while (cardNum <= 5) {
        randomNum = (int) ( Math.random() * ((maxValue - minValue) + 1) + minValue);
        
        //making sure that there are no repeat cards in the same hand
        if (randomNum == card1) {
          continue;
        } else if (randomNum == card2) {
          continue;
        } else if (randomNum == card3) {
          continue;
        } else if (randomNum == card4) {
          continue;
        } else if (randomNum == card5) {
          continue;
        }
        
        //assigning the randomly chosen number to each of the cards in the hand
        switch (cardNum) {
        case 1: 
            card1 = randomNum;
            break;
        case 2:
            card2 = randomNum;
            break;
        case 3:
            card3 = randomNum;
            break;
        case 4:
            card4 = randomNum;
            break;
        case 5:
            card5 = randomNum;
            break;
        default:
            break;
      }
        cardNum++;
      }
      
      //getting the card numbers to only the face values so they can be more easily analyzed
      card1 = (int) (1 + ((card1 - 1) % 13));
      card2 = (int) (1 + ((card2 - 1) % 13));
      card3 = (int) (1 + ((card3 - 1) % 13));
      card4 = (int) (1 + ((card4 - 1) % 13));
      card5 = (int) (1 + ((card5 - 1) % 13));
      
      //initialize and assign variables that will determine which type of hand exists
      int n1 = 0;
      int n2 = 0;
      int n3 = 0;
      int n4 = 0;
      
      //analyzing the cards for the different matching faces values
      if (card1 == card2) n1++; 
      if (card1 == card3) n1++;
      if (card1 == card4) n1++;
      if (card1 == card5) n1++;
      
      if (card2 == card3) n2++;
      if (card2 == card4) n2++;
      if (card2 == card5) n2++;
      
      if (card3 == card4) n3++;
      if (card3 == card5) n3++;
      
      if (card4 == card5) n4++;
      
      //analyzing the matches for the different types of hands
      if (n1 == 3 || n2 == 3) {
        fourOfAKind++;
      } else if (n1 == 2 || n2 == 2 || n3 == 2) {
        threeOfAKind++;
      } else if ((n1 + n2 + n3 + n4) == 1) {
        onePair++;
      } else if ((n1 + n2 + n3 + n4) > 1) {
        twoPair++;
      }
      
      //reseting the variables for the next hand to be analyzed
      cardNum = 0;
      counter++;
    }
    
    //initialize and assign the probability variables
    double probFourOfAKind = ((double) fourOfAKind / maxCount);
    double probThreeOfAKind = ((double) threeOfAKind / maxCount);
    double probTwoPair = ((double) twoPair / maxCount);
    double probOnePair = ((double) onePair / maxCount);
    
    //print out the probabilities and number of loops to the terminal
    System.out.println("The number of loops: " + maxCount);
    System.out.printf("The probability of Four-of-a-kind %.3f \n", probFourOfAKind);
    System.out.printf("The probability of Three-of-a-kind %.3f \n", probThreeOfAKind);
    System.out.printf("The probability of Two-pair %.3f \n", probTwoPair);
    System.out.printf("The probability of One-pair %.3f \n", probOnePair);

    
  } //end of main method
  
} //end of class