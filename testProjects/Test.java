/////////////////////////////////
// Spring 2013 Test 2 Question 2b
/////////////////////////////////
//Fall 2014 Test 2 Question 3
/////////////////////////////////
//Fall 2018 Test 2 Question 4
/////////////////////////////////
//Fall 2014 Test 1 Question 3
/////////////////////////////////

import java.util.Scanner;
import java.util.Arrays;

public class Test{
  public static void main(String[] args) {
    
    /*for (int i = 2; i < 5; i++) {
      for (int j = 1; j <= i; j++) {
        for (int k = 1; k <=j; k++) {
          System.out.print("b");
        }
        System.out.println("");
      }
    } */
    
   /*Scanner scan = new Scanner(System.in);
    int userNum1 = 0, userNum2 = 0;
    System.out.println("Enter an int: ");
    userNum1 = scan.nextInt();
    do {
      System.out.println("Enter an int greater than " + userNum1 + ":");
      userNum2 = scan.nextInt();
      if (userNum2 <= userNum1) {
        System.out.println("Sorry, you entered " + userNum2 + " <= " + userNum1 + "; try again");
      }
    } while (userNum2 <= userNum1);
    String userSum = sum(userNum1, userNum2);
    System.out.println(userSum); */
    
    /*int i = 0, j = 0;
    for (i = 0; i <= 9; i++) {
      if (i <= 4) {
        for (j = 0; j <= i; j++) {
          System.out.print(i);
        }
        System.out.println();
      }
      else {
        for (j = 0; j < 10 - i; j++) {
          System.out.print(i);
        }
        System.out.println();
      }
    } */
    
    /*int n = 9;
    int i = 0, j = 0, k = 0;
    if (n < 1 || n > 9) {
      System.out.println("out of range");
    } else if (n != 1) {
      for (i = 0; i <= n; i++) {
        if (i <= n / 2) { System.out.println("I'm an idiot."); }
        for (k = i; k > (int)(i / 2); k--) {
            System.out.print(" ");
          }
        for (j = 0; j <= i; j++) {
          System.out.print(i);
        }
        System.out.println();
      }
    } else { System.out.println("0"); }*/
    
    int[] array = {0, 1, 2, 3, 4, 5};
    int[] newArray1;
    int[] newArray2;
    int size1 = 8, size2 = 3;
    
    newArray1 = resize(array, size1);
    System.out.println("Array1: " + Arrays.toString(newArray1));
    
    newArray2 = resize(array, size2);
    System.out.println("Array2: " + Arrays.toString(newArray2));
    
  } //end of main method
  
  public static String sum(int x, int y) {
    
    String add = ("" + x);
    int sum = x + y;
    for (int i = x + 1; i <= y; i++) {
      String term = (" + " + i);
      add = add + term;
    }
    String equals = (" = " + sum);
    add = add + equals;
    return add;
    
  } //end of sum method
  
  public static int[] resize(int[] myArray, int newsize) {
    
    int[] array = new int[newsize];
    for(int i = 0; i < array.length; i++){
      if(i >= myArray.length){ array[i] = 0; }
      else{ array[i] = myArray[i]; }
    }
    return array;
    
  } //end of resize method
  
} //end of class