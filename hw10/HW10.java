/////
// CSE 02 HW10
// Jennifer Feeney
// December 04, 2018
/* this program plays Tic-Tac-Toe */
////

//imports the scanner and random classes into the program
import java.util.Scanner;
import java.util.Random;

public class HW10{ 
  //main method necessary for every program
  public static void main(String[] args) {
    
    Scanner myScanner = new Scanner(System.in); //assigning the scanner to the method
    
    String[][] board = {{"1", "2", "3"}, 
                        {"4", "5", "6"}, 
                        {"7", "8", "9"}}; //creating the board
    
    //initializing variables
    String playerPiece = " ", computerPiece = " "; //the pieces that will be placed on the board for a 1 player game
    String player1 = " ", player2 = " "; //the pieces that will be placed on the board in a 2 player game
    boolean determinate = false, keepGoing = false;
    String playerStatus = " "; //variable determining whether the user wants to play
    int gameType = 0; //value determining which type of game the user wants to play, 1 or 2 player
    int choice = 0; //value determining which piece goes to which player
    String badValue = " ", emptySpace = " ";
    
    System.out.println("Do you want to play a game?");
    playerStatus = myScanner.nextLine(); //asking the user to input anything they want into the terminal (no specific input necessary)
    
    //determining what the output to the terminal should be based on the user's inputed text
    if(playerStatus.equals("yes") || playerStatus.equals("Yes") || playerStatus.equals("y") || playerStatus.equals("Y")){
      System.out.println("Very good, indeed. Let us begin.");
      
    //if entering no, program will continue to run anyway against user wishes
    }else if(playerStatus.equals("no") || playerStatus.equals("No") || playerStatus.equals("n") || playerStatus.equals("N")){
      System.out.println("No, no, no. I don't think so. Let's play a game.");
      
    //if the user enters something other than yes or no, the computer will interpret it as a yes and run the program
    }else{ 
      System.out.println("Lucky for you, I understand what you meant kid. We'll continue with the game.");
    }
    
    
    System.out.println("What kind of game do you want to play? Eneter a 1 for 1-player or a 2 for 2-player: ");
    
    //determining what type of game the user wants to play, 1 or 2 players
    while(!keepGoing){
      determinate = myScanner.hasNextInt();
      
      //redo if the user doesn't enter an integer
      if(!determinate){
        System.out.println("Try entering an integer this time.");
        badValue = myScanner.nextLine();
        continue;
      }
      
      gameType = myScanner.nextInt();
      emptySpace = myScanner.nextLine();
      
      //place meanings on the entered values
      if(gameType == 1){
        System.out.println("You chose to play against me. Good luck.");
      }else if(gameType == 2){
        System.out.println("How unfortunate. Have fun playing against yourself...");
        System.out.println("... I probably would've beaten you anyway...");
      }else{ //redo if the user doesn't enter a valid option
        System.out.println("Try entering a valid game type this time?");
        continue;
      }
      keepGoing = true; //ends the loop
    }
    
    //plays the 2-player game
    if(gameType == 2){
      System.out.println("Player 1 is an X. Player 2 is an O. (deal with it)");
      player1 = "X";
      player2 = "O";
      
      doublePlayerGame(board, player1, player2);
    }
    
    //plays the 1-player game against a randomized computer
    if(gameType == 1){
      keepGoing = false; //allows the next loop to start
      
      //if the user is playing against the computer, they can determine which game piece they want to be
      System.out.println("Do you want to be an X or an O? Enter 1 for X and 2 for O: ");
    
      //while loop to determine which piece is assigned to which player
      while(!keepGoing){
        determinate = myScanner.hasNextInt();

        //redo user-input if the user doesn't enter an integer value
        if(!determinate){
          System.out.println("Try to enter an integer value this time.");
          badValue = myScanner.nextLine();
          continue;
        }

        choice = myScanner.nextInt();
        emptySpace = myScanner.nextLine();

        //determining which piece the players will play as
        if(choice == 1){
          System.out.println("You chose to be the X. I'm an O. Don't forget!");
          playerPiece = "X";
          computerPiece = "O";
        }else if(choice == 2){
          System.out.println("You chose to be the O. I'm an X. Don't forget!");
          playerPiece = "O";
          computerPiece = "X";
        }else{ //redo if the user-input isn't one of the valid options
          System.out.println("Try picking a valid option this time.");
          continue;
        }

        keepGoing = true; //stop the loop once everything has gone through without error
      }
      
      singlePlayerGame(board, playerPiece, computerPiece);
    }
    
  } //end of main method
  
  public static void singlePlayerGame(String[][] board, String playerPiece, String computerPiece) {
    
    Random random = new Random(); //adds the random number generator to the method
    
    int first = random.nextInt(1); //generates a random number between 0 and 1
    boolean winner = false;
    int turns = 0;
    
    //the user goes first
    if(first == 0){
      System.out.println("You're first.");
      print(board);
      
      //while loop to continue the game until someone wins or draw
      while(!winner){
        
        //the users turn
        playerMove(board, playerPiece);
        print(board);
        winner = determineWinner(board, playerPiece); //determining whether the user won
        if(winner){
          System.out.println("You win!");
          break; //game ends when someone wins
        }
        
        //marks the end of the user's turn
        turns++;
        if(turns == 9){
          System.out.println("The board is filled. It's a draw!");
          break;
        }
        System.out.println("Now it's my turn");
        
        //the computers turn
        computerMove(board, computerPiece);
        print(board);
        winner = determineWinner(board, computerPiece); //determining whether the computer won
        if(winner){
          System.out.println("I win!");
          break; //game ends when someone wins
        }
        
        //marks the end of the computer's turn
        turns++;
        if(turns == 9){
          System.out.println("The board is filled. It's a draw!");
          break;
        }
        System.out.println("Now it's your turn");
        
      }
    }
    
    //the computer goes first
    else{
      System.out.println("I'm first.");
      print(board);
      
      //while loop to continue the game until someone wins or draw
      while(!winner){
        
        //the computers turn
        computerMove(board, computerPiece);
        print(board);
        winner = determineWinner(board, computerPiece); //determining whether the computer won
        if(winner){
          System.out.println("I win!");
          break; //game ends when someone wins
        }
        
        //marks the end of the computer's turn
        turns++;
        if(turns == 9){
          System.out.println("The board is filled. It's a draw!");
          break;
        }
        System.out.println("Now it's your turn");
        
        //the users turn
        playerMove(board, playerPiece);
        print(board);
        winner = determineWinner(board, playerPiece); //determining whether the user won
        if(winner){
          System.out.println("You win!");
          break; //game ends when someone wins
        }
        
        //marks the end of the user's turn
        turns++;
        if(turns == 9){
          System.out.println("The board is filled. It's a draw!");
          break;
        }
        System.out.println("Now it's my turn");
      }
    }
    
  } //end of singlePlayerGame method
  
  public static void doublePlayerGame(String[][] board, String firstPlayerPiece, String secondPlayerPiece) {
    
    boolean winner = false;
    int turns = 0;
    
    //playing the game until someone wins or a draw
    while(!winner){
      
      //the first player's turn
      System.out.println("It's Player 1's turn. You're an X, if you couldn't remember.");
      print(board);
      playerMove(board, firstPlayerPiece);
      print(board);
      winner = determineWinner(board, firstPlayerPiece); //determining whether the user won
      if(winner){
        System.out.println("Player 1 wins. (meh, no one cares)");
        break; //game ends when someone wins
      }

      //marks the end of the first player's turn
      turns++;
      if(turns == 9){
        System.out.println("The board is filled. It's a draw. (do you even know how to play this game?)");
        break;
      }
      
      //the second player's turn
      System.out.println("It's Player 2's turn. You're an O, in case you forgot.");
      print(board);
      playerMove(board, secondPlayerPiece);
      print(board);
      winner = determineWinner(board, secondPlayerPiece); //determining whether the user won
      if(winner){
        System.out.println("Player 2 wins. (good job...yay, go you...)");
        break; //game ends when someone wins
      }

      //marks the end of the second player's turn
      turns++;
      if(turns == 9){
        System.out.println("The board is filled. It's a draw. (not suprising, you're not that great)");
        break;
      }
    }
    
  } //end of doublePlayerGame method
  
  public static void print(String[][] board) {
    
    //printing the board to the terminal for the user to see
    for(int row = 0; row < board.length; row++){
      for(int column = 0; column < board[row].length; column++){
        System.out.print(board[row][column] + "   ");
      }
      System.out.println();
    }
    
  } //end of printArray method
  
  public static void playerMove(String[][] board, String playerPiece) {
    
    Scanner myScanner = new Scanner(System.in); //assigning the scanner to the method
    
    //initializing variables
    boolean check = false, validity = false;
    String badValue = " ", emptySpace = " ";
    int position = 0;
    
    System.out.println("Make your move. Enter the number on the board you want to claim: ");
    
    //while loop to make sure the user enters a valid spot where they can place their game piece
    while(!validity){
      check = myScanner.hasNextInt();
      
      //redo if the user doesn't enter an integer value
      if(!check){
        System.out.println("Try entering an integer value this time.");
        badValue = myScanner.nextLine();
        continue;
      }
      
      position = myScanner.nextInt();
      emptySpace = myScanner.nextLine();
      
      validity = validateMove(board, position); //test whether the position is free on the game board
      
      //redo if the position the user wants already has a marker on it
      if(!validity){
        System.out.println("Try making a valid move this time.");
        continue;
      }
    }
    
    markBoard(board, playerPiece, position); //mark the move on the game board
    
  } //end of playerMove method
  
  public static void computerMove(String[][] board, String computerPiece) {
    
    Random random = new Random(); //assigning the random generator to the method
    
    boolean validity = false;
    int position = 0;
    
    //while loop to keep generating positions until a free one is found
    while(!validity){
      position = random.nextInt(9) + 1; //generates a random position on the board from 1 to 9
      validity = validateMove(board, position); //determining whether or not the space generated is free to play on
    }
    
    markBoard(board, computerPiece, position); //mark the move on the game board
    
  } //end of computerMove method
  
  public static boolean validateMove(String[][] board, int position) {
    
    //initializing variables
    String value = " ";
    boolean validity = false;
    
    //determining what the value of the spot on the board user is pointing at
    switch(position){
      case 1: value = board[0][0];
        break;
      case 2: value = board[0][1];
        break;
      case 3: value = board[0][2];
        break;
      case 4: value = board[1][0];
        break;
      case 5: value = board[1][1];
        break;
      case 6: value = board[1][2];
        break;
      case 7: value = board[2][0];
        break;
      case 8: value = board[2][1];
        break;
      case 9: value = board[2][2];
        break;
      default: value = "Error"; //if the user didn't enter a possibile position on the board
        break;
    }
    
    //determining whether the spot the user wants has already been taken
    if(value.equals("X")){ validity = false; }
    else if(value.equals("O")){ validity = false; }
    else if(value.equals("Error")){
      System.out.println("This isn't a space on the board.");
      validity = false;
    }else{ validity = true; }
    
    return validity;
    
  } //end of validateMove method
  
  public static void markBoard(String[][] board, String piece, int position) {
    
    //putting the game piece into the allocated spot on the game board
    switch(position){
      case 1: board[0][0] = piece;
        break;
      case 2: board[0][1] = piece;
        break;
      case 3: board[0][2] = piece;
        break;
      case 4: board[1][0] = piece;
        break;
      case 5: board[1][1] = piece;
        break;
      case 6: board[1][2] = piece;
        break;
      case 7: board[2][0] = piece;
        break;
      case 8: board[2][1] = piece;
        break;
      case 9: board[2][2] = piece;
        break;
      default: System.out.println("Major Error has occurred!!!");
        break;
    }
    
  } //end of markBoard method
  
  public static boolean determineWinner(String[][] board, String piece) {
    
    boolean winningStatus = false;
    
    //determining all of the different ways a player can win the game
    
    //winning horizontally
    if(board[0][0].equals(piece) && board[0][1].equals(piece) && board[0][2].equals(piece)){ winningStatus = true;}
    else if(board[1][0].equals(piece) && board[1][1].equals(piece) && board[1][2].equals(piece)){ winningStatus = true;}
    else if(board[2][0].equals(piece) && board[2][1].equals(piece) && board[2][2].equals(piece)){ winningStatus = true;}
    
    //wining vertically
    else if(board[0][0].equals(piece) && board[1][0].equals(piece) && board[2][0].equals(piece)){ winningStatus = true;}
    else if(board[0][1].equals(piece) && board[1][1].equals(piece) && board[2][1].equals(piece)){ winningStatus = true;}
    else if(board[0][2].equals(piece) && board[1][2].equals(piece) && board[2][2].equals(piece)){ winningStatus = true;}
    
    //winning diagonally
    else if(board[0][0].equals(piece) && board[1][1].equals(piece) && board[2][2].equals(piece)){ winningStatus = true;}
    else if(board[2][0].equals(piece) && board[1][1].equals(piece) && board[0][2].equals(piece)){ winningStatus = true;}
    
    else{ winningStatus = false; } //no winning placement found on the board
    
    return winningStatus;
    
  } //end of determineWinner method
  
} //end of class