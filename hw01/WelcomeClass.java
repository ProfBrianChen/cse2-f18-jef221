//////////////
//// CSE 02 Welcome Class
/// Jennifer Feeney
/// September 2, 2018
//
public class WelcomeClass{
  
  public static void main(String[] args){
    
    System.out.println("  -----------");
    System.out.println("  | WELCOME |");
    System.out.println("  -----------");
    ///prints the first three lines to the terminal window with the WELCOME message
    
    System.out.println("  ^  ^  ^  ^  ^  ^");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\ "); ///needed to double the backslashes used in the output. but will only appear once in the terminal window
    System.out.println("<-J--E--F--2--2--1->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /"); ///need to double the backslashes used in the output, but will only appear once in the terminal window
    System.out.println("  v  v  v  v  v  v");
    ///prints the next five lines to the terminal window with my personal Lehigh network ID as the message in the middle
    
    System.out.println("My name is Jennifer Feeney and I was born and raised in Bridgewater, New Jersey.");
    //prints an autobiographic statement that is 0-140 characters, or tweet-length
      
  }
  
}