/////
// CSE 02 CSE2Linear
// Jennifer Feeney
// November 16, 2018
/* this program allows the user to generate a list of 15 different final grades and then search on it */
////

//imports the scanner and random classes into the program
import java.util.Scanner;
import java.util.Random;

public class CSE2Linear{ 
  //main method necessary for every program
  public static void main(String[] args) {
    
    Scanner myScanner = new Scanner(System.in); //creating the scanner
    
    int[] finalGrades = new int[15]; //initializing the array for final grades
    
    //initializing values
    boolean arrayInput = false;
    String badValue = " ", enterValue = " ";
    int arrayValue = 0, previousArrayValue = 0;
    
    
    for(int i = 0; i < finalGrades.length; i++){
      while(i < finalGrades.length){
        
        /*differentiating between the different instructions for 
        the user based on what placement of the array they're in currently*/
        if(i == 0){
          System.out.println("Please enter a final grade in integer format between 0 and 100: ");
          arrayInput = myScanner.hasNextInt();
        }else if(i != 0){
          System.out.println("Please enter another grade greater than or equal to the last integer value: ");
          arrayInput = myScanner.hasNextInt();
        }
        
        //determining whether the user entered a valid integer value
        if(!arrayInput){
          badValue = myScanner.nextLine();
          System.out.println("Must input an integer value.");
          continue;
        }else if(arrayInput){
          arrayValue = myScanner.nextInt();
          enterValue = myScanner.nextLine();
          
          /*determining whether the integer is within 
          the correct boundaries for inclusion into the array*/
          if(arrayValue < 0 || arrayValue > 100){ 
            System.out.println("Must be between 0 and 100.");
            continue;
          }else if(arrayValue < previousArrayValue){
            System.out.println("Must be greater than the previously entered grade.");
            continue;
          }else{
            finalGrades[i] = arrayValue;
            previousArrayValue = arrayValue;
            break;
          }
        }
      }
    }
    
    print(finalGrades); //prints the array to the terminal
    
    //initializing values
    int searchValue1 = 0;
    boolean searchInput1 = false, tryAgain = false;
    
    //resetting values
    badValue = " "; 
    enterValue = " ";
    
    //while loop to get a final grade to search on
    while(!tryAgain){
      System.out.print("Please enter a grade you would like to search for: ");
      searchInput1 = myScanner.hasNextInt();
      System.out.println(); //add a blank line to the terminal to break up print statements
      
      //determining whether the user entered an integer value
      if(!searchInput1){
        badValue = myScanner.nextLine();
        System.out.println("Must be an integer value.");
      }else if(searchInput1){
        searchValue1 = myScanner.nextInt();
        enterValue = myScanner.nextLine();
        
        //determining whether the user entered an integer value within the valid range
        if(searchValue1 < 0 || searchValue1 > 100){
          System.out.println("Must be between 0 and 100.");
          continue;
        }else{ tryAgain = true; } //if the user did everything correctly, the loop will end
      }
    }
    
    int indexFound1 = binarySearch(finalGrades, searchValue1); //searching through the array for the user-inputed grade value
    
    //determining whether the final grade was found within the array
    if(indexFound1 == -1){
      System.out.println("However, the final grade of " + searchValue1 + " wasn't found.");
    }else{
      indexFound1 = indexFound1 + 1; //converting the index to a more "human" way of thinking (counting starting at 1 instead of 0)
      System.out.println("The final grade of " + searchValue1 + " was found at spot " + indexFound1 + ".");
    }
    
    System.out.println(); //add a blank line to the terminal to break up print statements
    
    //scramble and print out the array of final grades
    scrambleArray(finalGrades);
    print(finalGrades);
    
    //initializing values
    int searchValue2 = 0;
    boolean searchInput2 = false;
    
    //resetting values
    tryAgain = false;
    badValue = " "; 
    enterValue = " ";
    
    //while loop to get a final grade to search on
    while(!tryAgain){
      System.out.print("Please enter a grade you would like to search for: ");
      searchInput2 = myScanner.hasNextInt();
      System.out.println(); //add a blank line to the terminal to break up print statements
      
      //determining whether the user entered an integer value
      if(!searchInput2){
        badValue = myScanner.nextLine();
        System.out.println("Must be an integer value.");
      }else if(searchInput2){
        searchValue2 = myScanner.nextInt();
        enterValue = myScanner.nextLine();
        
        //determining whether the user entered an integer value within the valid range
        if(searchValue2 < 0 || searchValue2 > 100){
          System.out.println("Must be between 0 and 100.");
          continue;
        }else{ tryAgain = true; } //if the user did everything correctly, the loop will end
      }
    }
    
    int indexFound2 = linearSearch(finalGrades, searchValue2); //searching through the array for the user-inputed grade value
    
    //determining whether the final grade was found within the array
    if(indexFound2 == -1){
      System.out.println("However, the final grade of " + searchValue2 + " wasn't found.");
    }else{
      indexFound2 = indexFound2 + 1; //converting the index to a more "human" way of thinking (counting starting at 1 instead of 0)
      System.out.println("The final grade of " + searchValue2 + " was found at spot " + indexFound2 + ".");
    }
    
  } //end of main method
  
  public static void print(int[] array) {
    
    //for loop printing out an array
    for(int i = 0; i < array.length; i++){
      if(i == (array.length - 1)){ System.out.println(array[i] + "."); } //ends the array with a period
      else{ System.out.print(array[i] + ", "); } //seperates values within an array with commas
    }
    
  } //end of print method
  
  public static int linearSearch(int[] array, int key) {
    
    int counter = 0; //counts the number of itterations it takes to find the final grade the user is looking for is for
    
    //for loop searching for the first time the final grade appears in the array
    for(int i = 0; i < array.length; i++){
      counter++;
      if(key == array[i]){ 
        
        //determining if the word "iteration" needs to be plural or not
        if(counter == 1){
          System.out.println("With the linear search, it took " + counter + " iteration to find the final grade " + key + ".");
        }else{
          System.out.println("With the linear search, it took " + counter + " iterations to find the final grade " + key + ".");
        }
        
        return i; //returns the index of the final grade the user is looking for
      }
    }
    
    System.out.println("With the linear search, it took " + counter + " iterations to find the final grade " + key + ".");
    
    return -1; //if the final grade is never found, return a dummy value
    
  } //end of linearSearch method
  
  public static int binarySearch(int[] array, int key) {
    
    //initializing the high and low points of the array
    int lowValue = 0;
    int highValue = (array.length - 1);
    
    int counter = 0; //counts the number of itterations it takes to find the final grade the user is looking for is for
    
    //while loop searching for the final grade entered by the user through divide and conquer
    while(highValue >= lowValue){
      counter++;
      int midpoint = (lowValue + highValue) / 2; //split the array in half by finding its midpoint, or middle value
      
      //search to see if the key below or above the midpoint
      if(key < array[midpoint]){ highValue = midpoint - 1; } //limits the search area to all grades below the midpoint
      else if(key == array[midpoint]){ 
        
        //determining if the word "iteration" needs to be plural or not
        if(counter == 1){
          System.out.println("With the binary search, it took " + counter + " iteration to find the final grade " + key + ".");
        }else{ 
          System.out.println("With the binary search, it took " + counter + " iterations to find the final grade " + key + "."); 
        }
        
        return midpoint; //found when the final grade is the midpoint
      }else{ lowValue = midpoint + 1; } //limits the search area to all grades above the midpoint
    }
    
    System.out.println("With the binary search, it took " + counter + " iterations to find the final grade " + key + ".");
    
    return -1; //if the final grade is never found, return a dummy value
    
  } //end of binarySearch method
  
  public static void scrambleArray(int[] array) {
    
    Random random = new Random(); //creating the random generator
    
    //initializing variables
    int arrayLength = array.length; //saves the length of the array
    int saveValue = 0, randomNum = 0;
    
    //for loop to scramble the final grades within the array
    for(int i = 0; i < 100; i++){
      randomNum = random.nextInt(arrayLength); //randomly generates a number between 0 and (arrayLength - 1)
      saveValue = array[randomNum]; //saves the final grade associated with the random value
      
      //swaps the final grades in the random value spot and the first spot in the array
      array[randomNum] = array[0];
      array[0] = saveValue;
    }
    
  } //end of scrambleArray method
  
} //end of class