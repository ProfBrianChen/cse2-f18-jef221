/////
// CSE 02 RemoveElements
// Jennifer Feeney
// November 25, 2018
/* this program randomizes an array and allows the user 
to modify it by removing an index or value(s) inside the array */
////

//importing the scanner and random classes into the program
import java.util.Scanner;
import java.util.Random;

public class RemoveElements{
  //main method necessary for every program
  public static void main(String [] arg){ //taken from the code given in the homework spec
    
    Scanner scan=new Scanner(System.in); //creating the scanner
    
    //initializing variables
    int num[]=new int[10];
    int newArray1[];
    int newArray2[];
    int index,target;
    String answer="";
    
    do{
      System.out.print("Random input 10 ints [0-9]");
      num = randomInput();
      String out = "The original array is:";
      out += listArray(num);
      System.out.println(out);

      System.out.print("Enter the index ");
      index = scan.nextInt();
      newArray1 = delete(num,index);
      String out1="The output array is ";
      out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
      System.out.println(out1);

        System.out.print("Enter the target value ");
      target = scan.nextInt();
      newArray2 = remove(num,target);
      String out2="The output array is ";
      out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
      System.out.println(out2);

      System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
      answer=scan.next();
    }while(answer.equals("Y") || answer.equals("y"));
    
  } //end of main method

  public static String listArray(int num[]){ //taken from the code given in the homework spec
    String out= "{";
    
    for(int j=0;j<num.length;j++){
      if(j>0){ out+=", ";}
      out+=num[j];
    }
    
    out+="}";
    
    return out;
    
  } //end of listArray method

  public static int[] randomInput() {
    
    Random random = new Random(); //assigning the random class to the method
    
    //initializing variables
    int[] array = new int[10]; //initializing the new array
    int randomNum = 0;
    int arrayLength = array.length;
    
    //for loop to populate the array with random numbers
    for(int i = 0; i < arrayLength; i++){
      randomNum = random.nextInt(10); //randomizes a number between 0 and 9
      array[i] = randomNum;
    }
    
    return array;
    
  } //end of randomInput method
  
  public static int[] delete(int[] array, int position) {
    
    //initializing variables
    int[] newArray = new int[9]; //initializing the new array
    int arrayLength = array.length;
    int k = 0;
    
    //testing to see if the position is valid
    if(position < 0 || position > 9){ 
      System.out.println("The index is not valid.");
      return array; //if the position doesn't exist
    }else{
      
      //populating the newArray
      for(int i = 0; i < arrayLength; i++){
        if(i == position){ } //skipping position that the user doesn't want in their new array
        else{ 
          newArray[k] = array[i]; //the newArray is filled with the values of the starting array
          k++; //incrementing for the position of the new array
        }
      }
      
      return newArray; //returns the modified array without the position
    }
    
  } //end of delete method
  
  public static int[] remove(int[] array, int value) {
    
    //initializing variables
    int counter = 0;
    int arrayLength = array.length;
    int k = 0;
    
    //looking for the number of times the value occurs within the original array
    for(int i = 0; i < arrayLength; i++){
      if(value == array[i]){ counter++; }
    }
    
    //initializing the variables for the new array
    int newArrayLength = arrayLength - counter;
    int[] newArray = new int[newArrayLength]; //initializing the new array
    
    //for loop filling the modified array
    for(int j = 0; j < arrayLength; j++){
      if(value == array[j]){ } //skipping the value that the user doesn't want in their new array
      else{ 
        newArray[k] = array[j]; //the newArray is filled with the values of the starting array
        k++; //incrementing for the position of the new array
      }
    }
    
    return newArray; //returns the modified array without the value(s)
    
  } //end of the remove method
  
} //end of class