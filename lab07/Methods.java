/////
// CSE 02 Methods
// Jennifer Feeney
// October 26, 2018
/* this program builds a paragraph out of random words*/
////

//importing the scanner and random classes into the program
import java.util.Scanner;
import java.util.Random;

public class Methods{
	//main method necessary for every program
  public static void main(String[] args){
    
    Scanner myScanner = new Scanner(System.in); //assigns the scanner to the program
    Random randomGenerator = new Random(); //assigns the random number generator to the program
    
    //building the sentence for the first phase
    for (int x = 0; x >= 0; x++) {
      String subjectAdj = getAdjective();
      String verb = getPastVerb();
      String subjectNoun = getSubjectNoun();
      String objectNoun = getObjectNoun();
      String objectAdj = getAdjective();
      
      System.out.println("The " + subjectAdj + " " + subjectNoun + " " + verb + " the " + objectAdj + " " + objectNoun + ".");
      System.out.println("Would you like to generate a different sentence?");
      String userInput = myScanner.nextLine();
      
      //asking the user whether they want to build a new sentence
      if (userInput.equals("no") || userInput.equals("No")) {break;}
      else {continue;}
    }
    
    getParagraph(); //calling the method that builds phase 2
  
  } //end of main method
  
  public static String getAdjective() {
    
    Random randomGenerator = new Random(); //assigns the random number generator to the method
    
    int randomInt = randomGenerator.nextInt(10); //generates a random number between 0 and 9
    String adj = "";
    
    //associating a random number with an adjective
    switch (randomInt) {
      case 0: adj = "dead";
        break;
      case 1: adj = "scary";
        break;
      case 2: adj = "myseterious";
        break;
      case 3: adj = "weak";
        break;
      case 4: adj = "odd";
        break;
      case 5: adj = "grumpy";
        break;
      case 6: adj = "obnoxious";
        break;
      case 7: adj = "powerful";
        break;
      case 8: adj = "uninterested";
        break;
      case 9: adj = "rich";
        break;
      default: adj = "ERROR";
        break;
    }
    
    return adj;
    
  } //end of getAdjective method
  
  public static String getSubjectNoun() {
    
    Random randomGenerator = new Random(); //assigns the random number generator to the method
    
    int randomInt = randomGenerator.nextInt(10); //generates a random number between 0 and 9
    String subNoun = "";
    
    //associating a random number with a subject noun
    switch (randomInt) {
      case 0: subNoun = "superhero";
        break;
      case 1: subNoun = "magician";
        break;
      case 2: subNoun = "game developer";
        break;
      case 3: subNoun = "kidnaper";
        break;
      case 4: subNoun = "storyteller";
        break;
      case 5: subNoun = "heroin";
        break;
      case 6: subNoun = "muderer";
        break;
      case 7: subNoun = "theif";
        break;
      case 8: subNoun = "villain";
        break;
      case 9: subNoun = "hero";
        break;
      default: subNoun = "ERROR";
        break;
    }
    
    return subNoun;
    
  } //end of getSubjectNoun method
  
  public static String getObjectNoun() {
    
    Random randomGenerator = new Random(); //assigns the random number generator to the method
    
    int randomInt = randomGenerator.nextInt(10); //generates a random number between 0 and 9
    String obNoun = "";
    
    //associating a random number with an object noun
    switch (randomInt) {
      case 0: obNoun = "child";
        break;
      case 1: obNoun = "family";
        break;
      case 2: obNoun = "man";
        break;
      case 3: obNoun = "woman";
        break;
      case 4: obNoun = "person";
        break;
      case 5: obNoun = "student";
        break;
      case 6: obNoun = "girl";
        break;
      case 7: obNoun = "boy";
        break;
      case 8: obNoun = "mother";
        break;
      case 9: obNoun = "father";
        break;
      default: obNoun = "ERROR";
        break;
    }
    
    return obNoun;
    
  } //end of getObjectNoun method
  
  public static String getPresentVerb() {
    
    Random randomGenerator = new Random(); //assigns the random number generator to the method
    
    int randomInt = randomGenerator.nextInt(10); //generates a random number between 0 and 9
    String verb = "";
    
    //associating a random number with a present tense verb
    switch (randomInt) {
      case 0: verb = "target";
        break;
      case 1: verb = "visit";
        break;
      case 2: verb = "feed";
        break;
      case 3: verb = "steal";
        break;
      case 4: verb = "treat";
        break;
      case 5: verb = "beat";
        break;
      case 6: verb = "abandon";
        break;
      case 7: verb = "break";
        break;
      case 8: verb = "crawl";
        break;
      case 9: verb = "duel";
        break;
      default: verb = "ERROR";
        break;
    }
    
    return verb;
    
  } //end of getPresentVerb method
  
  public static String getAdVerb() {
    
    Random randomGenerator = new Random(); //assigns the random number generator to the method
    
    int randomInt = randomGenerator.nextInt(10); //generates a random number between 0 and 9
    String verb = "";
    
    //associating a random number with an adverb
    switch (randomInt) {
      case 0: verb = "almost";
        break;
      case 1: verb = "also";
        break;
      case 2: verb = "only";
        break;
      case 3: verb = "regualarly";
        break;
      case 4: verb = "brutally";
        break;
      case 5: verb = "sloppily";
        break;
      case 6: verb = "wholeheartedly";
        break;
      case 7: verb = "wickedly";
        break;
      case 8: verb = "quickly";
        break;
      case 9: verb = "delightfully";
        break;
      default: verb = "ERROR";
        break;
    }
    
    return verb;
    
  } //end of getAdVerb method
  
  public static String getPastVerb() {
    
    Random randomGenerator = new Random(); //assigns the random number generator to the method
    
    int randomInt = randomGenerator.nextInt(10); //generates a random number between 0 and 9
    String verb = "";
    
    //associating a random number with a past tense verb
    switch (randomInt) {
      case 0: verb = "hated";
        break;
      case 1: verb = "killed";
        break;
      case 2: verb = "mauled";
        break;
      case 3: verb = "stole from";
        break;
      case 4: verb = "kicked";
        break;
      case 5: verb = "punched";
        break;
      case 6: verb = "saved";
        break;
      case 7: verb = "broke";
        break;
      case 8: verb = "rescued";
        break;
      case 9: verb = "hugged";
        break;
      default: verb = "ERROR";
        break;
    }
    
    return verb;
    
  } //end of getPastVerb method
  
  public static String getThesisStatement() {
    
    //creating the parts of the thesis statement
    String subAdj = getAdjective();
    String verb = getPastVerb();
    String subNoun = getSubjectNoun();
    String obNoun = getObjectNoun();
    String obAdj = getAdjective();

    System.out.print("The " + subAdj + " " + subNoun + " " + verb + " the " + obAdj + " " + obNoun + ".");
    
    return subNoun;
  } //end of getThesisStatement method
  
  public static void getActionStatment(String subNoun) {
    
    Random randomGenerator = new Random(); //assigns the random number generator to the method
    
    int randomInt = randomGenerator.nextInt(2); //generates a random number between 0 and 1
    
    //creating the parts of the action statement(s)
    String subAdj = getAdjective();
    String obAdj = getAdjective();
    String pastVerb = getPastVerb();
    String presentVerb = getPresentVerb();
    String adVerb = getAdVerb();
    String obNoun1 = getObjectNoun();
    String obNoun2 = getObjectNoun();
    
    //determining whether the action sentence starts with "it" or the subject noun
    if(randomInt == 0) {
      System.out.print("This " + subNoun + " " + adVerb + " " + pastVerb + " a " + obAdj + " " + obNoun1 + ".");
    } else {
      System.out.print("It was " + presentVerb + "ing a " + obAdj + " " + obNoun1 + " when a " + obNoun2 + " " + pastVerb + " it.");
    }
    
  } //end of getActionStatment method
  
  public static void getConclusionStatement(String subNoun) {
    
    //creating the parts of the conclusion statement
    String pastVerb = getPastVerb();
    String obNoun = getObjectNoun();
    
    System.out.print("That " + subNoun + " " + pastVerb + " their " + obNoun + "!");
    
  }
  
  public static void getParagraph() {
    
    Random randomGenerator = new Random(); //assigns the random number generator to the method
    int randomInt = randomGenerator.nextInt(10); //generates a random number between 0 and 9
    
    String subjectNoun = getThesisStatement(); //adding the thesis statement to the paragraph
    
    //determining the number of action statements (randomly)
    for (int i = 0; i <= randomInt; i++) {
      System.out.print(" ");
      getActionStatment(subjectNoun); //adding an action statement to the paragraph
    }
    System.out.print(" ");
    getConclusionStatement(subjectNoun); //adding the conclusion sentence to the paragraph
    System.out.println("");
    
  } //end of getParagraph method
  
} //end of class