/////
// CSE 02 ReadingInputs
// Jennifer Feeney
// October 05, 2018
/* this program checks the input from the user with strings and integers 
to see if the user entered the correct information */
////

//importing the scanner class into the program
import java.util.Scanner;

public class ReadingInputs{
	//main method necessary for every program
  public static void main(String[] args){
    
    Scanner myScanner = new Scanner(System.in); //assigns the scanner to the program
    
    //initialize and assign variables the user will input
    int courseNum = 0;
    String departmentName = "Error";
    int numTimesPerWeek = 0;
    int timeStarts = 0;
    String instructorName = "Error";
    int numStudents = 0;
    
    //initialize and assing variables used to test conditions and absorb false values
    boolean checkValue1 = false;
    boolean checkValue2 = false;
    boolean checkValue3 = false;
    boolean checkValue4 = false;
    boolean checkValue5 = false;
    boolean checkValue6 = false;
    String badValue = "";
    String enterValue = "";
    
    /* start of the while statements that assign the user-entered data into the input variables */
    
    while (courseNum <= 0) {
      System.out.println("Please enter your course number: ");
      checkValue1 = myScanner.hasNextInt(); 
      
      if (!checkValue1) {
        badValue = myScanner.nextLine(); //the string badValue will take the non-integer the user entered
        System.out.println("You must enter a valid course number.");
        continue; //if the user enters a non-integer, it will re-run the while loop until the correct value is entered
      } 
      
      courseNum = myScanner.nextInt(); //assigns the integer value to courseNum
      enterValue = myScanner.nextLine(); //assigns the extra space when the user hits the enter key to enterValue
    }
    
    while (departmentName.equals("Error")) {
      System.out.println("Please enter the name of the department associated with the course: ");
      checkValue2 = myScanner.hasNextInt(); //checks to see whether the user entered an integer
      
      if (checkValue2) { 
        badValue = myScanner.nextLine(); //the string badValue will take the integer the user entered
        System.out.println("You must enter a valid department ID.");
        continue; //if the user enters an integer, it will re-run the while loop until the correct value is entered
      }
      
      departmentName = myScanner.nextLine(); //assigns the String value to departmentName
    }
    
    while (numTimesPerWeek <= 0) {
      System.out.println("Please enter the number of times the course meets per week: ");
      checkValue3 = myScanner.hasNextInt(); //checks to see whether the user entered an integer
      
      if (!checkValue3) { 
        badValue = myScanner.nextLine(); //the string badValue will take the non-integer the user entered
        System.out.println("You must enter a valid number.");
        continue; //if the user enters a non-integer, it will re-run the while loop until the correct value is entered
      } 
      
      numTimesPerWeek = myScanner.nextInt(); //assigns the integer value to numTimesPerWeek
      enterValue = myScanner.nextLine(); //assigns the extra space when the user hits the enter key to enterValue
    }
    
    while (timeStarts <= 0) {
      System.out.println("Please enter the start time of the course (in format HHMM): ");
      checkValue4 = myScanner.hasNextInt(); //checks to see whether the user entered an integer
      
      if (!checkValue4) {
        badValue = myScanner.nextLine(); //the string badValue will take the non-integer the user entered
        System.out.println("You must enter a valid start time in the proper format.");
        continue; //if the user enters a non-integer, it will re-run the while loop until the correct value is entered
      } 
      
      timeStarts = myScanner.nextInt(); //assigns the integer value to timeStarts
      enterValue = myScanner.nextLine(); //assigns the extra space when the user hits the enter key to enterValue
    }
    
    while (instructorName.equals("Error")) {
      System.out.println("Please enter the name of the instructor associated with the course: ");
      checkValue5 = myScanner.hasNextInt(); //checks to see whether the user entered an integer
      
      if (checkValue5) {
        badValue = myScanner.nextLine(); //the string badValue will take the integer the user entered
        System.out.println("You must enter a valid name.");
        continue; //if the user enters a integer, it will re-run the while loop until the correct value is entered
      }
      
      instructorName = myScanner.nextLine(); //assigns the String value to instructorName
    }
    
    while (numStudents <= 0) {
      System.out.println("Please enter the number of students in the course: ");
      checkValue6 = myScanner.hasNextInt(); //checks to see whether the user entered an integer
      
      if (!checkValue6) {
        badValue = myScanner.nextLine(); //the string badValue will take the non-integer the user entered
        System.out.println("You must enter a valid number.");
        continue; //if the user enters a non-integer, it will re-run the while loop until the correct value is entered
      } 
      
      numStudents = myScanner.nextInt(); //assigns the integer value to timeStarts
      enterValue = myScanner.nextLine(); //assigns the extra space when the user hits the enter key to enterValue
    }
    
    //the newly assigned user-entered input variables are printed to the terminal
    System.out.println("You are in " + departmentName + " " + courseNum + ".");
    System.out.println("This course meets " + numTimesPerWeek + " times per week.");
    System.out.println("It starts at " + timeStarts + ".");
    System.out.println("The instructor is " + instructorName + ".");
    System.out.println("There are " + numStudents + " students in the course.");
    
  } //end of main method
  
} //end of class