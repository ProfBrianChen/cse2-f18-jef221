/////
// CSE 02 CrapsIf
// Jennifer Feeney
// September 21, 2018
////
// this program takes two dice values, either entered 
// or randomly assigned, and outputs the slang term to the terminal,
// using only if else statements
////

//importing the scanner and math classes into the program
import java.lang.Math; 
import java.util.Scanner;

public class CrapsIf{
	//main method necessary for every program
  public static void main(String[] args){
    
    Scanner myScanner = new Scanner(System.in); //assigns the scanner to the program

    //sets the dice and terminology that is crucial for the output
    int dice1;
    int dice2;
    String slangTerm;
    
    //sets the user input which will determine the value of the dice
    String userInput1;
    String userInput2;
    
    //sets the boundaries of the random selection
    int maxValue = 6;
    int minValue = 1;
    
    //asks the user if they would like to set the values of the dice
    System.out.println("Would you like to set values for the dice? Type yes or no: ");
    userInput1 = myScanner.nextLine();
    
    /*determination of the value of the dice based on the previous user input
    yes = selection and no = randomization*/
    if (userInput1.equals("yes") || userInput1.equals("y")) {
      System.out.println("Please input the first die value (between 1 and 6): ");
      dice1 = myScanner.nextInt();
      
      System.out.println("Please input the second die value (betweent 1 and 6): ");
      dice2 = myScanner.nextInt();
    } else{
      System.out.println("Would you like to randomize the values? Type yes or no: ");
      userInput2 = myScanner.nextLine();
      if (userInput2.equals("yes") || userInput2.equals("y")){
        System.out.println("You have chosen to randmize the values.");
        dice1 = (int) (Math.random() * ((maxValue - minValue) + 1) + minValue);
        dice2 = (int) (Math.random() * ((maxValue - minValue) + 1) + minValue);
      } else{
        System.out.println("You have not entered a valid command. Please restart the program and try again.");
        dice1 = 0;
        dice2 = 0;
      }
    }
    
    //based on the previously determined dice value, a slang term is assigned
    if (dice1 == 0 && dice2 == 0){
      slangTerm = "";
    } else if (dice1 < 1 || dice2 < 1 || dice1 > 6 || dice2 > 6) {
      System.out.println("You tried to throw a " + dice1 + " and " + dice2 + ". You've been caught cheating!"); //prints out dice value and slang term to the terminal
    }
    else {
      if (dice1 == 1){ 
        if (dice2 == 1){
          slangTerm = "Snake Eyes"; 
        } else if (dice2 == 2){
          slangTerm = "Ace Deuce";
        } else if (dice2 == 3){  
          slangTerm = "Easy Four";
        } else if (dice2 == 4){  
          slangTerm = "Fever Five";
        } else if (dice2 == 5){  
          slangTerm = "Easy Six";
        } else if (dice2 == 6){
          slangTerm = "Seven Out";
        } else{   
          slangTerm = "Nonexistent"; 
        }
      } else if (dice1 == 2){ 
        if (dice2 == 1){
          slangTerm = "Ace Deuce"; 
        } else if (dice2 == 2){
          slangTerm = "Hard Four";
        } else if (dice2 == 3){  
          slangTerm = "Fever Five";
        } else if (dice2 == 4){  
          slangTerm = "Easy Six";
        } else if (dice2 == 5){  
          slangTerm = "Seven Out";
        } else if (dice2 == 6){
          slangTerm = "Easy Eight";
        } else{   
          slangTerm = "Nonexistent";
        }
      } else if (dice1 == 3){ 
        if (dice2 == 1){
          slangTerm = "Easy Four"; 
        } else if (dice2 == 2){
          slangTerm = "Fever Five";
        } else if (dice2 == 3){  
          slangTerm = "Hard Six";
        } else if (dice2 == 4){  
          slangTerm = "Seven Out";
        } else if (dice2 == 5){  
          slangTerm = "Easy Eight";
        } else if (dice2 == 6){
          slangTerm = "Nine";
        } else{   
          slangTerm = "Nonexistent";
        }
      } else if (dice1 == 4){ 
        if (dice2 == 1){
          slangTerm = "Fever Five"; 
        } else if (dice2 == 2){
          slangTerm = "Easy Six";
        } else if (dice2 == 3){  
          slangTerm = "Seven Out";
        } else if (dice2 == 4){  
          slangTerm = "Hard Eight";
        } else if (dice2 == 5){  
          slangTerm = "Nine";
        } else if (dice2 == 6){
          slangTerm = "Easy Ten";
        } else{   
          slangTerm = "Nonexistent";
        }
      } else if (dice1 == 5){ 
        if (dice2 == 1){
          slangTerm = "Easy Six"; 
        } else if (dice2 == 2){
          slangTerm = "Seven Out";
        } else if (dice2 == 3){  
          slangTerm = "Easy Eight";
        } else if (dice2 == 4){  
          slangTerm = "Nine";
        } else if (dice2 == 5){  
          slangTerm = "Hard Ten";
        } else if (dice2 == 6){
          slangTerm = "Yo-leven";
        } else{   
          slangTerm = "Nonexistent";
        }
      } else if (dice1 == 6){ 
        if (dice2 == 1){
          slangTerm = "Seven Out"; 
        } else if (dice2 == 2){
          slangTerm = "Easy Eight";
        } else if (dice2 == 3){  
          slangTerm = "Nine";
        } else if (dice2 == 4){  
          slangTerm = "Easy Ten";
        } else if (dice2 == 5){  
          slangTerm = "Yo-leven";
        } else if (dice2 == 6){
          slangTerm = "Boxcars";
        } else{   
          slangTerm = "Nonexistent";
        }
      } else{   
        slangTerm = "Nonexistent";
      }
      System.out.println("You threw a " + dice1 + " and " + dice2 + ". That's " + slangTerm + "!"); //prints out dice value and slang term to the terminal
    }
    
  } //end of main method
  
} //end of class