/////
// CSE 02 CrapsSwitch
// Jennifer Feeney
// September 22, 2018
////
// this program takes two dice values, either entered 
// or randomly assigned, and outputs the slang term to the terminal,
// using only switch statements
////

//importing the scanner and math classes into the program
import java.lang.Math; 
import java.util.Scanner;

public class CrapsSwitch{
	//main method necessary for every program
  public static void main(String[] args){
    
    Scanner myScanner = new Scanner(System.in); //assigns the scanner to the program

    //sets the dice and terminology that is crucial for the output
    int die1;
    int die2;
    String slangTerm;
    
    //sets the user input which will determine the value of the dice
    char userInput1;
    char userInput2;
    
    //sets the boundaries of the random selection
    int maxValue = 6;
    int minValue = 1;
    
    //asks the user if they would like to input the values for the dice
    System.out.println("Would you like to set values for the dice? Type y or n: ");
    userInput1 = myScanner.next().charAt(0);
    
    /*determines the values of the dice based on user inuput
    either randomization or user input will decide*/
    switch (userInput1){
      case 'y': //the user will input the dice values
        System.out.println("Please input the first die value (between 1 and 6): ");
        die1 = myScanner.nextInt();
        System.out.println("Please input the second die value (between 1 and 6): ");
        die2 = myScanner.nextInt();
        break;
      case 'n':
        System.out.println("Would you like to randomize the dice values? Type y or n: ");
        userInput2 = myScanner.next().charAt(0);
        switch (userInput2){
          case 'y': //the program will randomize the dice values
            System.out.println("You have chosen to randomize the dice values.");
            die1 = (int) (Math.random() * ((maxValue - minValue) + 1) + minValue);
            die2 = (int) (Math.random() * ((maxValue - minValue) + 1) + minValue);
            break;
          case 'n': 
            System.out.println("You have chosen to end the program.");
            die1 = 0;
            die2 = 0;
            break;
          default: 
            System.out.println("You have entered an invalid answer. Please restart the program and try again.");
            die1 = 0;
            die2 = 0;
            break;
        } 
        break;
      default: 
        System.out.println("You have entered an invalid answer. Please restart the program and try again.");
        die1 = 0;
        die2 = 0;
        break;
    }
    
    /*determines the slang terminology associated with the given dice values
    based off of the previous switch statement*/
    switch (die1){
      case 0: slangTerm = "";
        break;
      case 1: 
        switch (die2){
          case 1: slangTerm = "Snake Eyes";
            break;
          case 2: slangTerm = "Ace Deuce";
            break;
          case 3: slangTerm = "Easy Four";
            break;
          case 4: slangTerm = "Fever Five";
            break;
          case 5: slangTerm = "Easy Six";
            break;
          case 6: slangTerm = "Seven Out";
            break;
          default: slangTerm = "Cheating"; //die 2's value is less than one or greater than six
            break;
        }
        break;
      case 2: 
        switch (die2){
          case 1: slangTerm = "Ace Deuce";
            break;
          case 2: slangTerm = "Hard Four";
            break;
          case 3: slangTerm = "Fever Five";
            break;
          case 4: slangTerm = "Easy Six";
            break;
          case 5: slangTerm = "Seven Out";
            break;
          case 6: slangTerm = "Easy Eight";
            break;
          default: slangTerm = "Cheating"; //die 2's value is less than one or greater than six
            break;
        }
        break;
      case 3: 
        switch (die2){
          case 1: slangTerm = "Easy Four";
            break;
          case 2: slangTerm = "Fever Five";
            break;
          case 3: slangTerm = "Hard Six";
            break;
          case 4: slangTerm = "Seven Out";
            break;
          case 5: slangTerm = "Easy Eight";
            break;
          case 6: slangTerm = "Nine";
            break;
          default: slangTerm = "Cheating"; //die 2's value is less than one or greater than six
            break;
        }
        break;
      case 4: 
        switch (die2){
          case 1: slangTerm = "Fever Five";
            break;
          case 2: slangTerm = "Easy Six";
            break;
          case 3: slangTerm = "Seven Out";
            break;
          case 4: slangTerm = "Hard Eight";
            break;
          case 5: slangTerm = "Nine";
            break;
          case 6: slangTerm = "Easy Ten";
            break;
          default: slangTerm = "Cheating"; //die 2's value is less than one or greater than six
            break;
        }
        break;
      case 5: 
        switch (die2){
          case 1: slangTerm = "Easy Six";
            break;
          case 2: slangTerm = "Seven Out";
            break;
          case 3: slangTerm = "Easy Eight";
            break;
          case 4: slangTerm = "Nine";
            break;
          case 5: slangTerm = "Hard Ten";
            break;
          case 6: slangTerm = "Yo-leven";
            break;
          default: slangTerm = "Cheating"; //die 2's value is less than one or greater than six
            break;
        }
        break;
      case 6: 
        switch (die2){
          case 1: slangTerm = "Seven Out";
            break;
          case 2: slangTerm = "Easy Eight";
            break;
          case 3: slangTerm = "Nine";
            break;
          case 4: slangTerm = "Easy Ten";
            break;
          case 5: slangTerm = "Yo-leven";
            break;
          case 6: slangTerm = "Boxcars";
            break;
          default: slangTerm = "Cheating"; //die 2's value is less than one or greater than six
            break;
        }
        break;
      default: slangTerm = "Cheating"; //die 1's value is less than one or greater than six
        break;
    }
    
    /*determines whether or not the print statement with the dice values and slang terminology should be printed to the terminal
    cases where the values of the dice are zero will not be, but when the dice has a numerical value it will print*/
    switch (die1 + die2){
      case 0: //doesn't print statement to the terminal
        break;
      default:
        System.out.println("You rolled a " + die1 + " and " + die2 + ". That's " + slangTerm + "!"); //print statement to the terminal
        break;
    }
    
  } //end of main method
} //end of class