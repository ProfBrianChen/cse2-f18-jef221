/////
// CSE 02 Hw07
// Jennifer Feeney
// October 29, 2018
/* this program executes a number of menu options 
on a text entered into the program by the user */
////

//importing the scanner and random classes into the program
import java.util.Scanner;
import java.util.Random;

public class Hw07{
	//main method necessary for every program
  public static void main(String[] args){
    
    Scanner myScanner = new Scanner(System.in);
    
    String userInput = sampleText(); //calls and saves the user's text
    
    //initializing the counter needed for the loop
    int counter = 0;
    
    //loop to determine what actions will be taken based off of the menu options and will be executed on the user's text
    while (counter == 0) {
      char menuOption = printMenu(); //calls and saves the user's menu option choice
      
      //if the user wants to quit, the loop stops when the user enters q (only way to exit the loop)
      if (menuOption == 'q') {
        System.out.println("Exiting the program");
        break;
      }
      
      //testing the other menu options
      switch (menuOption) {
        case 'c': 
          int nonWhitespace = getNumOfNonWSCharacters(userInput); //tells the user the number of non-whitespace characters in their entered text
          System.out.println("Number of non-whitespace characters: " + nonWhitespace);
          break;
        case 'w': 
          int numWords = getNumOfWords(userInput); //tells the user the number of words in their entered text
          System.out.println("Number of words: " + numWords);
          break;
        case 'f':
          //prompts the user to pick the word that they want to program to search for
          System.out.println("Enter a word or phrase to be found: ");
          String find = myScanner.nextLine();
          int foundText = findText(find, userInput); //tells the user the number of times that the word the user entered appeared in their inputed text
          System.out.println(find + " instances: " + foundText);
          break;
        case 'r': 
          String replacedText = replaceExclamation(userInput); //shows the user the text is without the exclamation marks
          System.out.println("Edited text: " + replacedText);
          break;
        case 's': 
          String shortenedText = shortenSpaces(userInput); //shows the user the text without multiple unneccessary spaces
          System.out.println("Edited text: " + shortenedText);
          break;
        default: System.out.println("Enter a valid menu option."); //makes the user re-enter a menu option because their entry wasn't one of the valid options
          continue;
      }
      
    }
    
  } //end of main method
  
  public static String sampleText() {
    
    Scanner myScanner = new Scanner(System.in); //assigns the scanner to the program
    
    //prompts the user for a text that will be worked on later
    System.out.println("Enter a sample text: ");
    String userText = myScanner.nextLine();
    
    System.out.println("");
    System.out.println("You entered: " + userText); //spits the user's input back at them
    
    return userText; //sends the user's input to the main method 
    
  } //end of sampleText method
  
  public static char printMenu() {
    
    Scanner myScanner = new Scanner(System.in); //assigns the scanner to the program
    
    //prints out the menu options to the terminal
    System.out.println("");
    System.out.println("MENU");
    System.out.println("");
    System.out.println("c - Number of non-whitespace characters");
    System.out.println("w - Number of words");
    System.out.println("f - Find text");
    System.out.println("r - Replace all !'s");
    System.out.println("s - Shorten spaces");
    System.out.println("q - Quit");
    System.out.println("");

    System.out.println("Choose an option: ");
    String userInput = myScanner.nextLine(); //takes in the user's entery for the menu options
    
    char userChoice;
    
    if (userInput.length() == 1) {
      userChoice = userInput.charAt(0);
    } else {
      userChoice = 'a'; //if the user enters a non-character, a character is assigned so it can be returned to check for validity
    }
    
    return userChoice; //returns the character to the main method to test for validity
    
  } //end of printMenu method
  
  public static int getNumOfNonWSCharacters(String sampleText) {
    
    //initializing the varaibles necessary for the loop
    int length = sampleText.length();
    int counter = 0;
    char character;
    
    //loop that determines the number of characters in the user-entered text
    for (int i = 0; i < length; i++) {
      character = sampleText.charAt(i); //checks each letter of the text
      if (character == ' ') { } //makes sure that whitespaces aren't counted as characters
      else {counter++;} //counts the number of characters in the text
    }
    
    return counter;
    
  } //end of getNumOfNonWSCharacters method
  
  public static int getNumOfWords(String sampleText) {
    
    //initializing the varaibles necessary for the loop
    int length = sampleText.length();
    int counter = 0;
    char character, previousCharacter = 'a';
    
    //loop that determines the number of words in the user-entered text
    for (int i = 0; i < length; i++) {
      character = sampleText.charAt(i); //checks each letter of the text
      if (character == ' ' && previousCharacter != ' ') {counter++;} //if there is a space between two letters, then there is a word
      else if (i == length - 1) {counter++;} //if the letter is the last letter in the text, then there is a word 
      else { }
      previousCharacter = character;
    }
    
    return counter;
    
  } //end of getNumOfWords method
  
  public static int findText(String wordToFind, String sampleText) {
    
    //initializing variables needed in the loop
    int lengthWord = wordToFind.length();
    int lengthText = sampleText.length();
    int counterCharacters = 0;
    int counterWord = 0;
    char characterText, characterWord;
    
    //loop for the length of the text
    for (int i = 0; i < lengthText; i++) {
      characterText = sampleText.charAt(i);
      
      //if the first character in the word is found in the text, loop to see if that is the word the user is looking for
      if (characterText == wordToFind.charAt(0)) {
        counterCharacters = 0;
        
        //loop to check each character of the word with the next characters in the entered text
        for (int j = 1; j < lengthWord; j++) {
          characterWord = wordToFind.charAt(j);
          if ((i + j) >= lengthText) {break;} //cannot be the word if the text ends before the word would
          if (sampleText.charAt(i + j) == characterWord) {counterCharacters++;} //the likelihood of the word being there increases each time a the next letter matches
          else { }
        }
        
        //check to see whether the word was actually found in the text
        if (counterCharacters == lengthWord - 1) {
          if ((i + lengthWord) == lengthText) {counterWord++;} //if the word found in the text ends when the text ends, it must be the word the user was looking for
          
          //check the possible cases where the word found in the text would be the word the user was looking for if the next character isn't a letter
          else {
            switch(sampleText.charAt(i + lengthWord)) {
              case ' ': counterWord++;
                break;
              case ',': counterWord++;
                break;
              case ';': counterWord++;
                break;
              case '"': counterWord++;
                break;
              case '?': counterWord++;
                break;
              case '!': counterWord++;
                break;
              case '(': counterWord++;
                break;
              case '.': counterWord++;
                break;
              default: //if one of these cases isn't true, the word must not be the exact word that the user is looking for (could be part of another word)
                break; 
            }
          }
        }
      }
    }
    
    return counterWord;
    
  } //end of findText method
  
  public static String replaceExclamation(String sampleText) {
    
    //initializing variables needed for the loop
    int length = sampleText.length();
    String editedSampleText = "";
    char character;
    
    //loop to build the new user-entered text without exclamation points
    for (int i = 0; i < length; i++) {
      character = sampleText.charAt(i);
      if (character == '!') {character = '.';} 
      else { }
      editedSampleText += character; //adding each character to the edited text
    }
    
    return editedSampleText;
    
  } //end of replaceExclamation method
  
  public static String shortenSpaces(String sampleText) {
    
    //initializing the variables needed for the loop
    int length = sampleText.length();
    String editedSampleText = "";
    char character, previousCharacter = 'a';
    
    //loop to build the new user-entered text without extra spaces
    for (int i = 0; i < length; i++) {
      character = sampleText.charAt(i);
      if (character == ' ' && previousCharacter == ' ') { } //if there's a space after a space, don't add the second space to the edited text
      else {editedSampleText += character;} //if not, add the character to the edited text
      previousCharacter = character; //remember this character the for the next time through the loop
    }
    
    return editedSampleText;
    
  } //end of shortenSpaces method
  
} //end of class