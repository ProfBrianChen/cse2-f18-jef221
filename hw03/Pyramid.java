//////////////
//// CSE 02 Pyramid
/// Jennifer Feeney
/// September 14, 2018
//
//this program inputs the dimensions of a square pyramid and outputs the volume to the terminal

//must tell the program that the user will input in the terminal before writing the code for the program
import java.util.Scanner;

public class Pyramid{
  //main method required for every Java program
  public static void main(String[] args){
    
    Scanner myScanner = new Scanner(System.in); //constructs the scanner in the program
    
    //outputs a message to the terminal prompting the user to input the length of the pyramid and defines the variable to the scanner
    System.out.println("The square side of the pyramid is (input length): ");
    double pyramidLength = myScanner.nextDouble();
    
    //outputs a message to the terminal prompting the user to input the height of the pyramid and defines the variable to the scanner
    System.out.println("The height of the pyramid is (input height): ");
    double pyramidHeight = myScanner.nextDouble();
    
    double pyramidVolume; //defines the volume of the pyramid
    double pyramidBase; //defines the base of the pyramid
    
    pyramidBase = Math.pow(pyramidLength, 2); //calculate the base of the pyramid from the length of one side of the square
    pyramidVolume = ((pyramidBase * pyramidHeight) / 3); //calculate the volume of the pyramid
    
    System.out.println("The volume inside the pyramid is: " + pyramidVolume + "."); //output the volume to the terminal
    
  } //end of main method
  
} //end of class