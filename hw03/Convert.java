//////////////
//// CSE 02 Convert
/// Jennifer Feeney
/// September 14, 2018
//
//this program takes the area affected by a hurricane in acres and the amount 
//of rain in inches and outputs to the terminal the quantity of rain in cubic miles

//must tell the program that the user will input in the terminal before writing the code for the program
import java.util.Scanner;

public class Convert{
  //main method required for every Java program
  public static void main(String[] args){
    
    Scanner myScanner = new Scanner(System.in); //constructs the scanner in the program
    
    //outputs message to the terminal prompting the user to input the area affected by the hurrican and defines it to the scanner
    System.out.println("Enter the affected area in acres: ");
    double acresAffected = myScanner.nextDouble();
    
    //outputs message to the terminal prompting the user to input the inches of rainfall and defines it to the scanner
    System.out.println("Enter the rainfall in the affected area: ");
    double inchesRainfall = myScanner.nextDouble();
    
    double quantityOfRain;
    
    acresAffected = acresAffected * 43560; //converts acres to square feet
    acresAffected = acresAffected * (Math.pow(12, 2)); //converts square feet to square inches
    
    quantityOfRain = acresAffected * inchesRainfall; //calculates the quantity of rainfall in cubic inches
    quantityOfRain = quantityOfRain / (Math.pow(12, 3)); //converts cubic inches to cubic feet
    quantityOfRain = quantityOfRain / (Math.pow(5280, 3)); //converts cubic feet to cubic miles
   
    
    System.out.println(quantityOfRain + " cubic miles"); //outputs the quantity of rain in cubic miles to the terminal
    
  } //end of main method
  
} //end of class