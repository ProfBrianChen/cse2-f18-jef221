/////
// CSE 02 EncryptedX
// Jennifer Feeney
// October 20, 2018
/* this program hides an X in a user-chosen square through blank spaces */
////

//importing the scanner class into the program
import java.util.Scanner;

public class EncryptedX{
	//main method necessary for every program
  public static void main(String[] args){
    
    Scanner myScanner = new Scanner(System.in); //assigns the scanner to the program
    
    //initialize and assign variables assocaited with the square
    int userNum = 0;
    int numColumns, numRows;
    
    //initialize and assign variables assocaited with testing the userNum (used in the while loop)
    boolean checkValue = false;
    String badValue = "Error";
    String enterValue = "Error";
    
    
    /* determining the dimensions of the square where 
    the X will be hidden by asking the user for a number between 0 and 100 */
    
    while (userNum == 0) {
      System.out.println("Input a number between 0 and 100: ");
      checkValue = myScanner.hasNextInt();
      
      //check to make sure the user entered an integer value
      if (!checkValue) {
        badValue = myScanner.nextLine(); //the string badValue will take the non-integer the user entered
        System.out.println("Please input an integer.");
        continue; //if the user enters a non-integer, it will re-run the while loop until a correct value is entered
      }
      
      userNum = myScanner.nextInt(); //assigns the integer value to courseNum
      enterValue = myScanner.nextLine(); //assigns the extra space when the user hits the enter key to enterValue
      
      //check if the user entered a number between 0 and 100
      if (userNum < 0 || userNum > 100) {
        userNum = 0;
        System.out.println("Make sure your number is in range.");
        continue; //if the user enters an integer out of the desired range, it will re-run the while loop until a correct value is entered
      }
      userNum = userNum + 1; //done so that the X is embedded into the user-entered square instead of replacing values
      break; //leave the while loop
    }
    
    
    /* building the square based off of the 
    user-entered integer (assigned in the variable userNum) */
    
    //building the rows
    for (numRows = 1; numRows <= userNum; numRows++) {
      
      //building the columns
      for (numColumns = 1; numColumns <= userNum; numColumns++) {
        
        //assigns the blank spaces that'll build the X to the necessary cell(s)
        if (numColumns == numRows) {System.out.print(" ");}
        else if (numColumns == (userNum + 1 - numRows)) {System.out.print(" ");}
        
        //assigns the "*" that'll hide the X to the necessary cell(s)
        else {System.out.print("*");}
      }
      System.out.println(); //makes the next row
    }
    
  } //end of main method
  
} //end of class