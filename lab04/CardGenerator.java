/////
// CSE 02 CardGenerator
// Jennifer Feeney
// September 21, 2018
////
// this program picks a random card and outputs it to the user
////

//importing the scanner and math classes into the program
import java.lang.Math; 
import java.util.Scanner;

public class CardGenerator{
	//main method necessary for every program
  public static void main(String[] args){
    
    Scanner myScanner = new Scanner(System.in); //assigns the scanner to the program

    int minValue = 1; //sets maximum value
    int maxValue = 52; //sets minimum value

    //sets the random number and modified value (to be used later)
    int randomNum = (int) ( Math.random() * ((maxValue - minValue) + 1) + minValue);
    int newRandom;

    String cardSuit; //sets the suit of the card
    String cardIdentity; //sets the value of the card

    cardSuit = "";
    newRandom = 0;
    
    //If statements that will change the random number into corresponding suit and modify if needed
    if (randomNum <= 13) { 
    	cardSuit = "Diamonds";
    } //end of If statement

    if (randomNum >= 14 && randomNum <= 26) {
    	cardSuit = "Clubs";
    	newRandom = (int) (randomNum % 13);
    } //end of If statement

    if (randomNum >= 27 && randomNum <= 39) {
	    cardSuit = "Hearts";
	    newRandom = (int) (randomNum % 26);
    } //end of If statement

    if ( randomNum >= 40 ) {
	    cardSuit = "Spades";
	    newRandom = (int) (randomNum % 39);
    } //end of if statement

    //switch statement using the modified random number, changing it into corresponding card value
    switch ( newRandom ) {
		case 1: cardIdentity = "Ace";
			break;
		case 2: cardIdentity = "2";
			break;
		case 3: cardIdentity = "3";
			break;
		case 4: cardIdentity = "4";
			break;
		case 5: cardIdentity = "5";
			break; 			
        case 6: cardIdentity = "6";
			break;
		case 7: cardIdentity = "7";
			break;
		case 8: cardIdentity = "8";
			break;
		case 9: cardIdentity = "9";
			break;
		case 10: cardIdentity = "10";
			break;
		case 11: cardIdentity = "Jack";
			break;
		case 12: cardIdentity = "Queen";
			break;
		case 13: cardIdentity = "King";
			break;
		case 0: cardIdentity = "King";
			break;
		default: cardIdentity = "Invalid Card";
			break;
    } //end of switch statement

    //outputs randomly chosen card to the terminal
    System.out.println("You picked the " + cardIdentity + " of " + cardSuit + "."); 
    
  } //end of main method
} //end of class