/////
// CSE 02 PassingArrays
// Jennifer Feeney
// November 16, 2018
/* this program illustrates the effects 
of passing arrays as arguments in methods */
////

public class PassingArrays{ 
  //main method necessary for every program
  public static void main(String[] args) {
    
    //hardcoding in the first, or initial, array
    int[] array0 = {1, 2, 3, 4, 5, 6, 7, 8}; //I assume array0 should be hardcoded due to lack of information as to where to build this from
    int arrayLength = array0.length;
    
    //initializing the two arrays that will copy array0
    int[] array1 = new int[arrayLength];
    int[] array2 = new int[arrayLength];
    
    //copy array zero to build array1 and array2
    array1 = copy(array0);
    array2 = copy(array0);
    
    //manipulating and printing the values within array0
    inverter(array0);
    print(array0);
    
    //manipulating and printing the values within array1
    inverter2(array1);
    print(array1);
    
    //creating array3 from manipulating array2 and printing its values
    int[] array3 = inverter2(array2);
    print(array3);
    
  } //end of main method
  
  public static int[] copy(int[] array) {
    
    //initializing variables
    int arrayLength = array.length; //saves the length of the array as a value
    int[] copiedArray = new int[arrayLength];
    
    //for loop to copy the values from array into copiedArray
    for(int i = 0; i < arrayLength; i++){
      copiedArray[i] = array[i];
    }
    
    return copiedArray; //sends the newly built integer array back to the main method
    
  } //end of copy method
  
  public static void inverter(int[] array) {
    
    //initializing variables
    int arrayLength = array.length; //saves the length of the array as a value
    int saveValue = 0, lastValue = 0;
    
    //for loop to swap the values in an array
    for(int i = 0; i < arrayLength; i++){
      if(i <= ((arrayLength - 1)/2)){ //makes sure that the values aren't swapped twice by stopping at the midpoint of the array
        saveValue = array[i]; //saves the value associated with the ith spot in an array to swap later
        array[i] = array[arrayLength - 1 - i];
        array[arrayLength - 1 - i] = saveValue;
      }else{}
    }
    
  } //end of inverter method
  
  public static int[] inverter2(int[] array) {
    
    //initializing variables
    int arrayLength = array.length; //saves the length of the array as a value
    int[] newArray = new int[arrayLength];
    
    newArray = copy(array);
    inverter(newArray);
    
    return newArray;
    
  } //end of inverter2 method
  
  public static void print(int[] array) {
    
    //for loop printing out an array
    for(int i = 0; i < array.length; i++){
      if(i == (array.length - 1)){ System.out.println(array[i] + "."); } //ends the array with a period
      else{ System.out.print(array[i] + ", "); } //seperates values within an array with commas
    }
    
  } //end of print method
  
} //end of class