/////
// CSE 02 ArraysLab
// Jennifer Feeney
// November 09, 2018
/* this program creates an array of random numbers and 
tells the user how many times that number occurs within the array*/
////

public class ArraysLab{
	//main method necessary for every program
  public static void main(String[] args){
    
    //initialize and set the length of the two arrays
    int[] arrayRandom = new int[100];
    int[] arrayNumTimes = new int[100];
    
    /* initialize and assign variables associated with the mechanics
    of the for loop in order to fill the array with random numbers */
    int randomNum = 0;
    int maxValue = 99;
    int minValue = 0;
    
    //for loop to fill the array with random numbers and printing them to the terminal
    System.out.print("Array 1 holds the following integers: ");
    for (int i = 0; i < arrayRandom.length; i++) {
      randomNum = (int) ( Math.random() * ((maxValue - minValue) + 1) + minValue);
      arrayRandom[i] = randomNum;
      System.out.print(arrayRandom[i] + " ");
    }
    System.out.println(); //ending the sequence of printing the numbers
    
    //for loop filling the second array with the number of times each of the random number options occurs
    for (int num = 0; num <= 99; num++) {
      int counter = 0;
      for (int j = 0; j < arrayRandom.length; j++) {
        if (arrayRandom[j] == num) { counter++; }
      }
      arrayNumTimes[num] = counter;
    }
    
    //for loop to print the second array to the terminal in a more legible format
    System.out.println(); //adding a space between the first and second arrays when printed to the terminal
    for (int k = 0; k < arrayNumTimes.length; k++) {
      if (arrayNumTimes[k] == 0) { }
      else if (arrayNumTimes[k] == 1) {
        System.out.println(k + " occurs " + arrayNumTimes[k] + " time");
      } else {
        System.out.println(k + " occurs " + arrayNumTimes[k] + " times");
      }
    }
    
  } //end of main method
  
} //end of class