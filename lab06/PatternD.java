/////
// CSE 02 PatternD
// Jennifer Feeney
// October 11, 2018
/* this program builds the pyramids with pattern D */
////

//importing the scanner class into the program
import java.util.Scanner;

public class PatternD{
	//main method necessary for every program
  public static void main(String[] args){
    
    Scanner myScanner = new Scanner(System.in); //assigns the scanner to the program
    
    int numLines = 0; //initialize and assign the length of the pyramid
    
    //user assigns the number of lines that they want the pyramid to be
    while (numLines == 0) {
      System.out.println("Please enter the length of the pyramid you would like to build (integer from 1 to 10):");
      numLines = myScanner.nextInt();
      
      //determines whether or not the user-intputed value is a number between 1 and 10
      if (numLines < 1 || numLines > 10) {
        System.out.println("Please input a number between 1 and 10.");
        numLines = 0;
        continue;
      }else {
        break;
      }
    }
    
    //outputs the pyramid in pattern D to the screen
    System.out.println("");
    for (int numRows = 1; numRows <= numLines; numRows++) { //sets the looks of the rows of the pyramid
      for (int numColumns = (1 + numLines - numRows); numColumns >= 1; numColumns--) { //sets the look of the columns of the pyramid
        System.out.print("" + numColumns + " ");
      }
      System.out.println(""); //makes sure each row ends up on a new line
    }
    
  } //end of main method
  
} //end of class 