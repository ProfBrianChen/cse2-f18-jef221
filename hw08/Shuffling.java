/////
// CSE 02 Shuffling
// Jennifer Feeney
// November 13, 2018
/* this program shuffles a deck of cards and then generates a hand */
////

//import the scanner to the class
import java.util.Scanner;

public class Shuffling{ 
  //main method necessary for every program
  public static void main(String[] args) {
    
    Scanner scan = new Scanner(System.in); //initializing the scanner
    
    //suits club, heart, spade or diamond 
    String[] suitNames={"C","H","S","D"};    
    String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
    
    //initializing the arrays and variables to be used with the deck of cards
    String[] cards = new String[52];
    int again = 1; 
    int index = 51;
    
    //for loop creating the different cards in the deck
    for (int i=0; i<52; i++){ 
      cards[i]=rankNames[i%13]+suitNames[i/13]; 
      System.out.print(cards[i]+" "); 
    } 
    System.out.println();
    
    //putting cards through the different methods
    printArray(cards); 
    shuffle(cards); //shuffling the cards
    printArray(cards);
    
    //initializing the array and variable associated with the the hand to be delt from the deck
    int numCards = 0;
    while (numCards == 0){
      System.out.println("How many cards do you want in your hand?");
      numCards = scan.nextInt();
      
      //makes sure that the user doesn't ask for more cards in their hand than in a normal deck, or less
      if (numCards <= 0 || numCards > 52) {
        System.out.println("Please enter a valid number for your hand.");
        numCards = 0;
      } else {
        System.out.println("Here's your hand...");
      }
    }
    String[] hand = new String[numCards]; 
    
    //while loop to determine the hand that the user will recieve  
    while(again == 1){ 
      
      //reshuffles a deck and puts the index back to the beginning
      if (index < numCards) {
        System.out.println("A new deck is in play.");
        shuffle(cards);
        index = 51;
      }
      
      hand = getHand(cards,index,numCards); //generates a hand for the user
      printArray(hand);
      index = index - numCards;
      
      //allowing the user to generate a new hand
      System.out.println("Enter a 1 if you want another hand drawn"); 
      again = scan.nextInt(); 
    }  
    
  } //end of main method 
  
  public static void printArray(String[] list) {
    
    //for loop to print out the cards in the deck
    for (int j = 0; j < list.length; j++) {
      System.out.print(list[j] + " ");
    }
    
    System.out.println(); //starts the next print statement on a new line
    
  } //end of printArray method
  
  public static void shuffle(String[] list) {
    
    //declaring and initializing variables for the for loop
    String saveValue = " ";
    int randomCard = 0;
    int maxValue = 51;
    int minValue = 1;
    
    //for loop to swap the cards in the deck
    for(int k = 0; k < 100; k++){
      randomCard = (int) ( Math.random() * ((maxValue - minValue) + 1) + minValue); //creates a radom number from 1 to 51
      saveValue = list[randomCard]; //saves the value of the randomly selected card
      
      //swaps the values from the first card on the deck with a random card in the deck
      list[randomCard] = list[0];
      list[0] = saveValue;
    }
    
    System.out.println("Shuffled"); //print statement telling the user that the deck has been shuffled
    
    
  } //end of shuffle method
  
  public static String[] getHand(String[] list, int index, int numCards) {
    
    String[] hand = new String[numCards]; //creating a new array for the hand
    
    //for loop assinging the cards from the deck to the hand
    for(int n = 0; n < numCards; n++){
      hand [n] = list[index - n];
    }
    
    return hand; //returns the new array to the main method
    
  } //end of getHand
  
} //end of class
