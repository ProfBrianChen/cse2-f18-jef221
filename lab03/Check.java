//////////////
//// CSE 02 Check
/// Jennifer Feeney
/// September 14, 2018
//
//this program takes the check from a resturaunt and outputs the total amount 
//that each person at the dinner has to pay with the users chosen tip percentage

//must tell the program that the user will input in the terminal before writing the code for the program
import java.util.Scanner;

public class Check{
  //main method required for every Java program
  public static void main(String[] args){
    
    Scanner myScanner = new Scanner(System.in); //assigns the scanner to the program
    
    //print a message prompting a user to enter the amount of money the check costs in the terminal and assign it to the scanner
    System.out.println("Enter the original cost of the check in the form xx.xx: ");
    double checkCost = myScanner.nextDouble();
    
    //print a message promting the user to enter the tip percentage that they want to use in the terminal and assign it to the scanner
    System.out.println("Enter the percentage tip that you wish to pay as a whole number (in the form xx): ");
    double tipPercent = myScanner.nextDouble();
    tipPercent /= 100; //we want to convert the percentage into a decimal value
    
    //print a message promting the user to enter the number of people splitting the bill and assign it to the scanner
    System.out.println("Enter the number of people who went out to dinner: ");
    int numPeople = myScanner.nextInt();
    
    //assign output variables
    double totalCost;
    double costPerPerson;
    int dollars, //whole dollar amount of cost
          dimes, pennies; //for storing digits to the right of the decimal point for the cost$
    
    //calculations for the total cost and cost per person
    totalCost = checkCost * (1 + tipPercent);
    costPerPerson = totalCost / numPeople;
    //get the whole amount, dropping decimal fraction
    dollars = (int) costPerPerson;
    //get dimes amount, e.g.,
    //(int) (6.73 * 10) % 10 -> 67 % 10 -> 7
    //where the % (mod) operator returns the remainder
    //after the division:   583 % 100 -> 83, 27 % 5 -> 2
    dimes = (int) (costPerPerson * 10) % 10;
    pennies = (int) (costPerPerson * 100) % 10;
    
    System.out.println("Each person in the group owes $" + dollars + "." + dimes + pennies); //output message
    
  } //end of main method
  
} //end of class